clc
close all
clear 

%% Import data
[PlaceName,January,July] = importfile('../data/rainfall_data.csv',2, 65);

%% Find and remove the mean
fprintf('Mean Jan %f\n', mean(January))
fprintf('Mean July %f\n', mean(July))
January = January - mean(January);
July = July - mean(July);

%% Compute the cov matrix
X = [January, July];
C = cov(X);
disp(C)

%% Find the eigenvectors (PCs) and eigenvalues
[v, diag] = eig(C);
p2 = v(:,1);
p1 = v(:,2);

%% Find the projections
proj1 = X*p1;
proj2 = X*p2;

%% Plot
x = -40:40;

figure()
% 1
subplot(1,2,1); 
title('Original Data');
xlabel('January'), ylabel('July')
hold on
grid on
scatter(January, July);
y = p1(2)/p1(1)*x;
plot(x,y)
y = p2(2)/p2(1)*x;
plot(x,y)
axis([-40 40 -40 40])
hold off
% 2
subplot(1,2,2),scatter(proj1, proj2)
title('Projected Data');