#include "pca.h"

namespace PNCJAK001{

std::ostream& operator<<(std::ostream &os, const Datapoint &d){
    os << d.location << ": (" << d.rainfall_jan << "," << d.rainfall_july << ")";
    return os;
}

bool Dataset::read(std::string filename)
{
    using namespace std;

    // ---- Use .dat to find dimensions
    ifstream inputData(filename);
    if(!inputData){
        cout << "Couldn't open " << filename << endl;
        return false;
    }
    
    string token, temp;
    vector<double> jan;
    vector<double> july;
    
    // remove first line
    getline(inputData, temp);

    while(inputData >> temp)
    {
        Datapoint d;
        stringstream s(temp);
        
        getline(s, temp, ';');
        d.location = temp;
        getline(s, temp, ';');
        d.rainfall_jan = atof(temp.c_str());
        getline(s, temp, ';');
        d.rainfall_july = atof(temp.c_str());

        data.push_back(d);
    }
    inputData.close();

    
    return true;
}

void Dataset::print(){
    for(auto i : data)
        std::cout << i << std::endl;

}

void Dataset::centerData(){
    // Find means
    double s_jan = 0, s_july = 0;
    for(Datapoint d : data)
    {
        s_jan += d.rainfall_jan;
        s_july += d.rainfall_july;
    }

    double av_jan = s_jan/data.size();
    double av_july = s_july/data.size();

    std::cout << "averages " << av_jan << " "<< av_july << std::endl;

    // Center data
    for(int i= 0; i < data.size(); ++i)
    {
        data[i].rainfall_jan -= av_jan;
        data[i].rainfall_july -= av_july;
    }

}

double Dataset::covariance(){
    double sum = 0;
    for(Datapoint d: data)
    {
        sum += d.rainfall_jan*d.rainfall_july;
    }

    return sum/(data.size() - 1);
}

double Dataset::variance(std::string label){
    double sum = 0;

    if (label == "jan"){
        for(Datapoint d: data)
            sum += d.rainfall_jan*d.rainfall_jan;
    }
    else if(label == "july"){
        for(Datapoint d: data)
            sum += d.rainfall_july*d.rainfall_july;
    }
    else{
        std::cout << "Incorrect label provided" << std::endl;
    }
    
    return sum/(data.size() - 1);
}

}