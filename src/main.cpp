#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include "pca.h"

// using Eigen::MatrixXd;
using namespace std;

const string DATA_DIR = "./data/";
const string FILENAME = "./rainfall_data.csv";

int main()
{
    PNCJAK001::Dataset d;

    // ---- Read in data
    d.read(DATA_DIR + FILENAME);

    // ---- Center the data
    d.centerData();

    // ---- Populate the covariance matrix
    double cov, var1, var2;
    cov = d.covariance();
    var1 = d.variance("jan");
    var2 = d.variance("july");
    cout <<"covariance:\t"<< d.covariance() << endl;
    cout <<"variance jan:\t"<<d.variance("jan") << endl;
    cout <<"variance july:\t"<<d.variance("july") << endl;

    Eigen::MatrixXd m(2, 2);
    m(0, 0) = var1;
    m(0, 1) = cov;
    m(1, 0) = cov;
    m(1,1) = var2;
    cout << "\n----Covariance Matrix---- \n" << m << endl;
    
    // ---- Compute the eigenvalues and eigenvectors
    Eigen::ComplexEigenSolver< Eigen::MatrixXd > ces;
    ces.compute(m);
    cout << "\n----Eigenvalues----\n" << ces.eigenvalues().real() << endl;
    cout << "\n----Matrix of eigenvectors:----\n" << ces.eigenvectors().real() << endl << endl;

    cout << "PC1's eigenvalue is:" << endl << ces.eigenvalues()[1].real() << endl;
    cout << "PC2's eigenvalue is:" << endl << ces.eigenvalues()[0].real() << endl;
    cout << "PC1 is:\n" << ces.eigenvectors().col(1) << endl << endl;
    cout << "PC2 is:\n" << ces.eigenvectors().col(0) << endl << endl;

    // ---- Create the output file and answer the questions
    string output_filename = "answers.txt";
    ofstream os(output_filename);
    if(!os){
        cout << "Couldn't open " << output_filename << endl;
        return 1;
    }

    os << "QUESTION 1:" << endl;
    os << " * PC1 has the eigenvalue: "  << ces.eigenvalues()[1].real() << endl;
    os << " * PC2 has the eigenvalue: "  << ces.eigenvalues()[0].real() << endl;

    os << "\nQUESTION 2:" << endl;
    os << " * PC1 eigenvector: \n"  << ces.eigenvectors().col(1).real() << endl;
    os << "PC1 components: "  
            << ces.eigenvectors().col(1).row(0).real() << " January, "
            << ces.eigenvectors().col(1).row(1).real() << " July"<< endl;
    os << " * PC2 eigenvector: \n"  << ces.eigenvectors().col(0).real() << endl;
    os << "PC2 components: "  
            << ces.eigenvectors().col(0).row(0).real() << " January, "
            << ces.eigenvectors().col(0).row(1).real() << " July"<< endl;

    os << "\nQUESTION 3:" << endl;
    os << " * Covariance matrix:\n" << m << endl;

    os << "\nQUESTION 4:" << endl;
    double variance = ces.eigenvalues()[1].real() + ces.eigenvalues()[0].real();
    os <<" * Total variance: " << variance << endl;

    os << "\nQUESTION 5:" << endl;
    os << " * PC1 explains: "  << ces.eigenvalues()[1].real()/variance*100 << " %" << endl;
    os << " * PC2 explains: "  << ces.eigenvalues()[0].real()/variance*100 << " %" << endl;

    return 0;

}