#ifndef _PCA
#define _PCA

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

namespace PNCJAK001{

class Datapoint
{
    public:
        double rainfall_july, rainfall_jan;
        std::string location;

        friend std::ostream& operator<<(std::ostream &os, const Datapoint &d);
}; 


class Dataset
{
   
    public:
        std::vector<Datapoint> data;

        Dataset():data(){};
        
        bool read(std::string filename);
        void print();
        void centerData();
        double covariance();
        double variance(std::string label);

};


}//end namespace

#endif //_PCA


